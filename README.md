# Climbing App - User Interface

This is the frontend project for a climbing information web app. It comprises of a Node.js (TypeScript) server and an Angular app on the client-side. Details of how to build the UI can be found below, along with instructions of how to run the full climbing app using Docker Compose.

## Prerequisites
The instructions below refer specifically to running the climbing app using Docker and Docker Compose. Alongside this, there are several other tools etc that are needed to carry out the various steps. The instructions therefore assume the reader has installed each of the following:
- Java
- Maven
- Angular
- TypeScript
- Node.js
- Node package manager
- Docker
- Docker Compose

The reader can also choose where to run the application - a Windows host (Docker for Windows), a virtual machine or a Linux host.

## Running the Climbing App
The climbing app is composed of the following components:
- [Location Service](https://gitlab.com/chris-smith/climbing-app-location-service) - a Spring Boot Java microservice that is used to retrieve and store location data e.g. find your nearest climbing centres
- [Weather Service](https://gitlab.com/chris-smith/climbing-app-weather-service) - a Spring Boot Java microservice that retrieves weather-related information e.g. the current or forecasted weather at a specific crag
- [User Service](https://gitlab.com/chris-smith/climbing-app-user-service) - a Spring Boot Java microservice that handles registration and authentication of users
- UI server - a Node.js server written in TypeScript
- UI client - an Angular application

The following steps will outline how to get all of the above components running together on your machine.

### Step 1 - Copy code for each service into one folder
Create a new folder called 'ClimbingApp' and then do the following:
- Download the 'LocationService' and 'database' folders from [here](https://gitlab.com/chris-smith/climbing-app-location-service/-/tree/master) - rename the database folder to 'LocationDatabase' and then copy both folders to the 'ClimbingApp' folder
- Download the 'WeatherService' folder from [here](https://gitlab.com/chris-smith/climbing-app-weather-service/-/tree/master) and then copy it into the 'ClimbingApp' folder
- Download the 'UserService' and 'database' folders from [here](https://gitlab.com/chris-smith/climbing-app-user-service/-/tree/master) - rename the database folder to 'UserDatabase' and then copy both folders to the 'ClimbingApp' folder
- Download the 'client' and 'server' folders from [here](https://gitlab.com/chris-smith/climbing-app-user-interface/-/tree/master) and then copy both folders to the 'ClimbingApp' folder

### Step 2 - Build each service
The next step is to compile/build each of the individual components:
- Open a command prompt/terminal and navigate to the newly-created 'ClimbingApp' folder e.g. if using a Windows machine and the 'ClimbingApp' folder was created at the root of the C drive, a command that could be used is `cd C:\ClimbingApp`
- Navigate into the 'LocationService' folder (`cd LocationService`) and run the command `mvn clean install`
- Navigate into the 'WeatherService' folder (`cd ..\WeatherService`) and run the command `mvn clean install`
- Navigate into the 'UserService' folder (`cd ..\UserService`) and run the command `mvn clean install`
- Navigate into the 'server' folder (`cd ..\server`) and run the command `npm install` (to install the dependencies), followed by `npm run build` (to compile the Node app)
- Navigate into the 'client' folder (`cd ..\client`) and run the command `npm install` (to install the dependencies), followed by `npm run build` (to compile the Angular app)

### Step 3 - Create a docker-compose file
The next step is to create a docker-compose.yml file for the whole climbing application:
- Navigate back to the 'ClimbingApp' folder and run one of the following commands to create an empty docker-compose.yml file
   - If on Windows - `echo > docker-compose.yml`
   - If on Linux - `touch docker-compose.yml`
- Open the docker-compose.yml file you created above
- Copy the below text into the file
```
version: "3.9"
services:
  databasehost:
    image: mariadb
    environment:
      - MYSQL_ROOT_PASSWORD=rootpw
    ports:
      - "3306:3306"
  
  zookeeperhost:
    image: zookeeper:3.6.2
    ports:
      - "2181:2181"
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes

  kafkahost:
    image: wurstmeister/kafka:2.12-2.5.0
    ports:
      - "9092:9092"
    environment:
      - KAFKA_ZOOKEEPER_CONNECT=zookeeperhost:2181
      - ALLOW_PLAINTEXT_LISTENER=yes
      - KAFKA_ADVERTISED_HOST_NAME=kafkahost
      - KAFKA_CREATE_TOPICS=weather-update:1:1
    depends_on:
      - zookeeperhost

  # Location Service projects
  location-database:
    build: LocationDatabase
    environment:
      - WAIT_FOR_MYSQL=true
      - MYSQL_ROOT_PASSWORD=rootpw
      - LOCATION_USER_PASSWORD=location-user-password
    depends_on:
      - databasehost

  location-service:
    build: LocationService
    ports:
      - "9091:9091"
    depends_on:
      - location-database

  # Weather Service projects
  weather-service:
    build: WeatherService
    ports:
      - "9090:9090"
    depends_on:
      - kafkahost

  # User Service projects
  user-database:
    build: UserDatabase
    environment:
      - WAIT_FOR_MYSQL=true
      - MYSQL_ROOT_PASSWORD=rootpw
      - USER_DB_USER_PASSWORD=user-db-user-password
    depends_on:
      - databasehost

  user-service:
    build: UserService
    ports:
      - "9093:9093"
    depends_on:
      - user-database

  # User Interface projects
  ui-server:
    build: server
    ports:
      - "3000:3000"
    depends_on:
      - kafkahost
    deploy:
      restart_policy:
        condition: on-failure
        max_attempts: 20

  ui-client:
    build: client
    ports:
      - "4200:4200"
```

### Step 4 - Build the docker images
If using a virtual machine, copy the 'ClimbingApp' folder over to it now - if running the app on a Linux host (or Windows locally), this is not needed. Using the docker-compose file you created above, build an image for each container that is specified in the file. This can be done by navigating to the 'ClimbingApp' folder (where the docker-compose file is located) and running the command `docker-compose build`.

### Step 5 - Launch all of the containers
The final step is to run all of the Docker containers, using the images created above. This can be done by running the command `docker-compose up` from within the same folder. Once the logs have slowed down/everything appears to have booted up, you should be able to access the web app by opening a browser and navigating to `<VIRTUAL_MACHINE_IP_ADDRESS>:4200` (if using a VM) or `localhost:4200` (if running locally). You should now be able to create an account or log in and explore the site.
