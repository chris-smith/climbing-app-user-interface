import { HttpClient, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { tapResponse } from "@ngrx/component-store";
import { ReplaySubject } from "rxjs";
import { shareReplay } from "rxjs/operators";
import { ApiRoute } from "../_enums/api-route";
import { LocationType } from "../_enums/location-type";
import { IClimbingLocation } from "../_interfaces/climbing-location";
import { ILocationDetails } from "../_interfaces/location-details";
import { ILocationWeather } from "../_interfaces/location-weather";
import { INearbyPlace } from "../_interfaces/nearby-place";
import { INewClimbingLocationRequest } from "../_interfaces/new-climbing-location-request";
import { LocalStorageService } from "./local-storage.service";
import { SettingsService } from "./settings.service";

@Injectable({ providedIn: 'root' })
export class LocationService {
    savedOutdoorClimbingLocations: ILocationWeather[] = [];
    savedIndoorClimbingLocations: IClimbingLocation[] = [];

    outdoorLocationSearchResult$ = new ReplaySubject<ILocationDetails>(1);
    savedOutdoorClimbingLocationsUpdated$ = new ReplaySubject<ILocationWeather[]>(1);
    indoorLocationsSearchResult$ = new ReplaySubject<INearbyPlace[]>(1);
    savedIndoorClimbingLocationsUpdated$ = new ReplaySubject<IClimbingLocation[]>(1);

    constructor(
        private http: HttpClient,
        private settingsService: SettingsService,
        private localStorageService: LocalStorageService) { }

    /**
     * Performs a search to retrieve location details for the location with the specified name
     * @param locationName the location to search for
     */
    searchForOutdoorLocation(locationName: string): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.LocationSearch}`;
        const params = new HttpParams()
            .set("token", this.localStorageService.getAuthToken())
            .set("locationName", locationName);

        this.http.get<ILocationDetails>(apiUrl, { params }).pipe(
            tapResponse(
                (res: ILocationDetails) => {
                    this.outdoorLocationSearchResult$.next(res);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            ),
            shareReplay(1)
        ).subscribe();
    }

    /**
     * Makes a request to save a new climbing location to the database for a user
     * @param placeId the unique identifier of the location/place that is to be saved,
     * as returned from Google Maps' Place Search API
     * @param isOutdoorLocation a boolean indicating whether it is an outdoor climbing location or not
     */
    saveClimbingLocation(placeId: string, isOutdoorLocation: boolean): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.SaveLocation}`;
        const newLocationRequest: INewClimbingLocationRequest = {
            userId: this.localStorageService.getAuthToken(),
            placeId: placeId,
            outdoorLocation: isOutdoorLocation
        };

        if (isOutdoorLocation) {
            this.http.post<ILocationWeather>(apiUrl, newLocationRequest)
                .pipe(
                    tapResponse(
                        (res: ILocationWeather) => {
                            this.savedOutdoorClimbingLocations.push(res);
                            this.savedOutdoorClimbingLocationsUpdated$.next(this.savedOutdoorClimbingLocations);
                        },
                        (err: HttpErrorResponse) => {
                            console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                        }
                    )
                ).subscribe();
        } else {
            this.http.post<IClimbingLocation>(apiUrl, newLocationRequest)
                .pipe(
                    tapResponse(
                        (res: IClimbingLocation) => {
                            this.savedIndoorClimbingLocations.push(res);
                            this.savedIndoorClimbingLocationsUpdated$.next(this.savedIndoorClimbingLocations);
                        },
                        (err: HttpErrorResponse) => {
                            console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                        }
                    )
                ).subscribe();
        }        
    }

    /**
     * Makes a request to remove a location from a user's saved locations
     * @param locationId the unique identifier of the location that is to be removed from the user's saved locations
     * @param isOutdoorLocation a boolean indicating whether the location to be removed is an outdoor
     * climbing location or not
     */
    removeLocationForUser(locationId: number, isOutdoorLocation: boolean): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.RemoveLocation}`;
        const params = new HttpParams()
            .set("token", this.localStorageService.getAuthToken())
            .set("locationId", locationId.toString());

        this.http.delete(apiUrl, { params })
            .pipe(
                tapResponse(
                    () => {
                        if (isOutdoorLocation) {
                            this.savedOutdoorClimbingLocations = this.savedOutdoorClimbingLocations.filter(l => l.locationId !== locationId);
                            this.savedOutdoorClimbingLocationsUpdated$.next(this.savedOutdoorClimbingLocations);
                        } else {
                            this.savedIndoorClimbingLocations = this.savedIndoorClimbingLocations.filter(l => l.locationId !== locationId);
                            this.savedIndoorClimbingLocationsUpdated$.next(this.savedIndoorClimbingLocations);
                        }
                    },
                    (err: HttpErrorResponse) => {
                        console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                    }
                )
            ).subscribe();        
    }

    /**
     * Retrieves the saved outdoor climbing locations for the logged-in user
     */
    getSavedOutdoorClimbingLocations(): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.OutdoorClimbingLocations}`;
        const params = new HttpParams().set("token", this.localStorageService.getAuthToken());
        this.http.get<ILocationWeather[]>(apiUrl, { params }).pipe(
            tapResponse(
                (res: ILocationWeather[]) => {
                    this.savedOutdoorClimbingLocations = res;
                    this.savedOutdoorClimbingLocationsUpdated$.next(this.savedOutdoorClimbingLocations);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            )
        ).subscribe();
    }

    /**
     * Retrieves the saved indoor climbing locations for the logged in user
     */
    getSavedIndoorClimbingLocations(): void {        
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.IndoorClimbingLocations}`;
        const params = new HttpParams().set("token", this.localStorageService.getAuthToken());
        this.http.get<IClimbingLocation[]>(apiUrl, { params }).pipe(
            tapResponse(
                (res: IClimbingLocation[]) => {
                    this.savedIndoorClimbingLocations = res;
                    this.savedIndoorClimbingLocationsUpdated$.next(this.savedIndoorClimbingLocations);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            )
        ).subscribe();
    }

    /**
     * Performs a search to retrieve the nearest places of the specified type to the specified location
     * @param locationName the location to find the nearest places of the specified type to
     * @param locationType the type of location to search for
     */
    getNearestLocations(locationName: string, locationType: LocationType): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.GetNearestLocations}`;
        const params = new HttpParams()
            .set("token", this.localStorageService.getAuthToken())
            .set("locationName", locationName)
            .set("locationType", locationType);

        this.http.get<INearbyPlace[]>(apiUrl, { params }).pipe(
            tapResponse(
                (res: INearbyPlace[]) => {
                    this.indoorLocationsSearchResult$.next(res);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            )
        ).subscribe();
    }
}
