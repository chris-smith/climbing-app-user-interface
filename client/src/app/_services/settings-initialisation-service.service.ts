import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Settings } from "src/settings";
import { SettingsService } from "./settings.service";

@Injectable({ providedIn: 'root' })
export class SettingsInitialisationService {
     
    constructor(private http: HttpClient, private settingsService: SettingsService) { }

    initialiseApp(): Promise<void> {
        return new Promise(
            (resolve) => {
                this.http.get("assets/settings.json")
                .toPromise()
                .then((response: Settings) => {
                    this.settingsService.settings = response;
                    this.settingsService.settings.serverUrl = `${window.location.protocol}//${this.settingsService.settings.serverUrl}`;
                    resolve();
                })
            }
        )
    }
}
