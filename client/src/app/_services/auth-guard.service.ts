import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Route } from "../_enums/route";
import { AuthService } from "./auth.service";

@Injectable({ providedIn: 'root' })
export class AuthGuardService implements CanActivate {

    constructor(public authService: AuthService, public router: Router) { }

    /**
     * @returns a boolean indicating whether the user is authorised to access a route
     */
    canActivate(): boolean {
        if (!this.authService.isLoggedIn()) {
            this.router.navigateByUrl(Route.Login);
            return false;
        }
        return true;
    }
}
