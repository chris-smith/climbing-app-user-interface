import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Observable } from "rxjs";
import { shareReplay } from "rxjs/operators";
import { ApiRoute } from "../_enums/api-route";
import { IChangePasswordRequest } from '../_interfaces/change-password-request';
import { ILoginRequest } from "../_interfaces/login-request";
import { IRegisterUserRequest } from '../_interfaces/register-user-request';
import { LocalStorageService } from './local-storage.service';
import { SettingsService } from "./settings.service";
import { WeatherService } from './weather.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
    userAuthenticated = new EventEmitter<boolean>();

    constructor(
        private http: HttpClient,
        private jwtHelper: JwtHelperService,
        private settingsService: SettingsService,
        private localStorageService: LocalStorageService,
        private weatherService: WeatherService) { }

    /**
     * Returns a boolean indicating whether the user's auth token is still valid and
     * has not expired
     */
    isLoggedIn(): boolean {
        try {
            const isTokenExpired = this.jwtHelper.isTokenExpired();
            this.userAuthenticated.emit(!isTokenExpired);
            return !isTokenExpired;
        } catch (e) {
            console.error("Failed to check if user's auth token is still valid.");
            console.error(e);
            return false;
        }
    }
    
    /**
     * Makes a request to the UI server to register a new user
     * @param registerUserRequest the request object containing the user's credentials
     */
    register(registerUserRequest: IRegisterUserRequest): Observable<any> {
        return this.http.post<any>(
            `${this.settingsService.settings.serverUrl}${ApiRoute.Register}`, 
            registerUserRequest)
            .pipe(
                shareReplay()
        );
    }

    /**
     * Makes a request to the UI server to log a user in
     * @param loginRequest the request object containing the user's credentials
     */
    logIn(loginRequest: ILoginRequest): Observable<any> {
        return this.http.post<any>(
            `${this.settingsService.settings.serverUrl}${ApiRoute.LogIn}`, 
            loginRequest)
            .pipe(
                shareReplay()
        );
    }

    /**
     * Logs the user out
     */
    logOut(): void {
        this.weatherService.stopWeatherUpdates();
        this.localStorageService.removeAuthToken();
    }

    /**
     * Makes a request to the UI server to change the user's password
     * @param changePasswordRequest the request object containing the user's existing and
     * new password
     */
    changePassword(changePasswordRequest: IChangePasswordRequest): Observable<any> {
        return this.http.post<any>(
            `${this.settingsService.settings.serverUrl}${ApiRoute.ChangePassword}`, 
            changePasswordRequest);
    }

    /**
     * Sets the user's auth token via the local storage service
     * @param authToken the user's JWT
     */
    setToken(authToken: string): void {
        this.localStorageService.setAuthToken(authToken);
    }

    /**
     * Decodes the user's JWT and returns the user's ID
     * @returns the unique identifier for the user
     */
    getUserId(): string {
        const decodedToken = this.jwtHelper.decodeToken(this.localStorageService.getAuthToken());
        return decodedToken?.userId;
    }
}
