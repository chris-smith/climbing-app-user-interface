import { Injectable } from "@angular/core";
import { ReplaySubject } from "rxjs";

@Injectable({ providedIn: 'root' })
export class NavigationService {
    routeChange$ = new ReplaySubject<any>(1);

    constructor() { }
}
