import { Injectable } from "@angular/core";
import { AppConstants } from "../_constants/app-constants";

@Injectable({ providedIn: 'root' })
export class LocalStorageService {

    constructor() { }

    /**
     * Gets the user's JWT from local storage
     */
    getAuthToken(): string {
        return localStorage.getItem(AppConstants.LOCAL_STORAGE_AUTH_TOKEN_KEY);
    }

    /**
     * Sets the user's JWT in local storage
     * @param token the user's JWT
     */
    setAuthToken(token: string): void {
        localStorage.setItem(AppConstants.LOCAL_STORAGE_AUTH_TOKEN_KEY, token);
    }

    /**
     * Removes the user's JWT from local storage
     */
    removeAuthToken(): void {
        localStorage.removeItem(AppConstants.LOCAL_STORAGE_AUTH_TOKEN_KEY);
    }
}
