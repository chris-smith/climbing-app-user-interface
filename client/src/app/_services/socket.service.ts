import { Injectable, OnDestroy } from "@angular/core";
import { ReplaySubject, Subject } from "rxjs";
import { io, Socket } from "socket.io-client";
import { ISocketMessage } from "../_interfaces/socket-message";
import { AuthService } from "./auth.service";
import { LocalStorageService } from "./local-storage.service";
import { SettingsService } from "./settings.service";

@Injectable({ providedIn: 'root' })
export class SocketService implements OnDestroy {
    private socket: Socket;
    
    onDestroy$ = new Subject<void>();
    newMessage$ = new ReplaySubject<ISocketMessage>(1);
    rooms: string[] = [];

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        private settingsService: SettingsService) {

        let socketConfig: any = {
            query: `token=${this.localStorageService.getAuthToken()}`
        }

        this.socket = io(`${this.settingsService.settings.serverUrl}/`, socketConfig);
        this.socket.on("connect", (error?: any): void => this.onConnect(error));
        this.socket.on("message", (message: ISocketMessage) => {
            this.newMessage$.next(message);
        });
    }

    ngOnDestroy(): void {
        this.disconnect();
        this.onDestroy$.next();
    }

    onConnect = (error?: any): void => {
        if (error) {
            console.error(error);
            return;
        }

        if (this.rooms.length >= 1) {
            this.socket.emit("join", this.rooms);
        }

        console.log(`Connected - socket ID: ${this.socket.id}`);
    }

    joinRoom = (room: string): void => {
        if (room && !this.rooms.includes(room)) {
            this.socket.emit("join", room);
            this.rooms.push(room);
        }
    }

    leaveRoom = (room: string): void => {
        if (room && this.rooms.includes(room)) {
            this.socket.emit("leave", room);
            this.rooms.splice(this.rooms.indexOf(room), 1);
        }
    }

    connect = (): void => {
        if (this.authService.isLoggedIn()) {
            this.socket.connect();
        }
    }

    disconnect = (): void => {
        this.socket.disconnect();
    }
}
