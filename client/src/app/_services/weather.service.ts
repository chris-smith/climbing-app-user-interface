import { HttpClient, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { tapResponse } from "@ngrx/component-store";
import { EMPTY, ReplaySubject } from "rxjs";
import { catchError } from "rxjs/operators";
import { ApiRoute } from "../_enums/api-route";
import { IDailyWeatherForecast } from "../_interfaces/daily-weather-forecast";
import { IHourlyWeatherForecast } from "../_interfaces/hourly-weather-forecast";
import { LocalStorageService } from "./local-storage.service";
import { SettingsService } from "./settings.service";

@Injectable({ providedIn: 'root' })
export class WeatherService {
    hourlyWeatherForecast$ = new ReplaySubject<IHourlyWeatherForecast[]>(1);
    dailyWeatherForecast$ = new ReplaySubject<IDailyWeatherForecast[]>(1);

    constructor(
        private http: HttpClient,
        private settingsService: SettingsService,
        private localStorageService: LocalStorageService) { }

    /**
     * Retrieves hourly and daily weather forecasts for the specified latitude/longitude
     * @param latitude the latitude of the location to get the forecasts for
     * @param longitude the longitude of the location to get the forecasts for
     */
    getWeatherForecasts(latitude: string, longitude: string): void {
        this.getHourlyWeatherForecast(latitude, longitude);
        this.getDailyWeatherForecast(latitude, longitude);
    }

    /**
     * Retrieves an hourly weather forecast for the next 48 hours, for the specified
     * latitude/longitude
     * @param latitude the latitude of the location to get the hourly forecast for
     * @param longitude the longitude of the location to get the hourly forecast for
     */
    getHourlyWeatherForecast(latitude: string, longitude: string): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.HourlyWeatherForecast}`;
        const params = new HttpParams()
            .set("token", this.localStorageService.getAuthToken())
            .set("latitude", latitude)
            .set("longitude", longitude);

        this.http.get<IHourlyWeatherForecast[]>(apiUrl, { params }).pipe(
            tapResponse(
                (res: IHourlyWeatherForecast[]) => {
                    this.hourlyWeatherForecast$.next(res);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            )
        ).subscribe();
    }

    /**
     * Retrieves a daily weather forecast for the next 7 days, for the specified
     * latitude/longitude
     * @param latitude the latitude of the location to get the daily forecast for
     * @param longitude the longitude of the location to get the daily forecast for
     */
    getDailyWeatherForecast(latitude: string, longitude: string): void {
        const apiUrl = `${this.settingsService.settings.serverUrl}${ApiRoute.DailyWeatherForecast}`;
        const params = new HttpParams()
            .set("token", this.localStorageService.getAuthToken())
            .set("latitude", latitude)
            .set("longitude", longitude);

        this.http.get<IDailyWeatherForecast[]>(apiUrl, { params }).pipe(
            tapResponse(
                (res: IDailyWeatherForecast[]) => {
                    this.dailyWeatherForecast$.next(res);
                },
                (err: HttpErrorResponse) => {
                    console.error(`Error occurred while making request to ${apiUrl} - error message: ${err.message}`);
                }
            )
        ).subscribe();
    }

    /**
     * Starts the real-time weather updates for the logged-in user, with the default
     * interval between updates (in seconds) as configured on the server
     */
    startWeatherUpdates(): void {
        this.http.post<any>(
            `${this.settingsService.settings.serverUrl}${ApiRoute.StartWeatherUpdates}`, 
            {
                token: this.localStorageService.getAuthToken()
            })
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    console.error(`Error occurred while attempting to start weather updates - error message: ${err.message}`);
                    return EMPTY;
                })
            ).subscribe();
    }

    /**
     * Stops the real-time weather updates for the logged-in user
     */
     stopWeatherUpdates(): void {
        this.http.post<any>(
            `${this.settingsService.settings.serverUrl}${ApiRoute.StopWeatherUpdates}`, 
            {
                token: this.localStorageService.getAuthToken()
            })
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    console.error(`Error occurred while attempting to stop weather updates - error message: ${err.message}`);
                    return EMPTY;
                })
            ).subscribe();
    }
}
