import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { GetAuthTokenResponse } from 'src/app/_enums/get-auth-token-response';
import { Route } from 'src/app/_enums/route';
import { AuthService } from 'src/app/_services/auth.service';
import { LocationService } from 'src/app/_services/location.service';
import { WeatherService } from 'src/app/_services/weather.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  invalidForm = false;
  invalidCredentials = false;
  usernameAlreadyTaken = false;
  unexpectedError = false;

  constructor(
    private fb: FormBuilder, 
    private router: Router,
    private authService: AuthService,
    private locationService: LocationService,
    private weatherService: WeatherService) {
    this.form = this.fb.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
   }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl(Route.OutdoorClimbing);
    }
  }

  /**
   * Makes a login request to the server
   */
  async logIn(): Promise<void> {
    this.resetValidationBools();
    const currentFormValue = this.form.value;
    const username = currentFormValue.username;
    const password = currentFormValue.password;
    
    if (username && password) {
      let res;
      try {
        res = await this.authService.logIn({ username: username, password: password }).pipe().toPromise();
      } catch (e) {
        res = e.error;                
      }

      this.handleLoginResponse(res);
    } else {
      this.invalidForm = true;
    }
  }

  /**
   * Makes a request to the server to register a new user
   */
  async register(): Promise<void> {
    this.resetValidationBools();
    const currentFormValue = this.form.value;
    const username = currentFormValue.username;
    const password = currentFormValue.password;
    
    if (username && password) {
      let res;
      try {
        res = await this.authService.register({ username: username, password: password }).pipe().toPromise();
      } catch (e) {
        res = e.error;                
      }

      this.handleLoginResponse(res);
    } else {
      this.invalidForm = true;
    }
  }

  /**
   * Handles the response from the UI server for a login or register-new-user request
   * @param res the response from the UI server
   */
  private handleLoginResponse(res: any): void {
    switch (res.responseStatus) {
      case GetAuthTokenResponse.OK:
        this.authService.setToken(res.token);
        this.locationService.getSavedOutdoorClimbingLocations();
        this.locationService.getSavedIndoorClimbingLocations();
        this.weatherService.startWeatherUpdates();
        this.router.navigateByUrl(Route.OutdoorClimbing);
        break;
      case GetAuthTokenResponse.INVALID_CREDENTIALS:
        this.invalidCredentials = true;
        break;
      case GetAuthTokenResponse.USERNAME_ALREADY_TAKEN:
        this.usernameAlreadyTaken = true;
        break;
      default:
        this.unexpectedError = true;
        console.log(`Unexpected error occurred. Response status: ${res.responseStatus}.`)
        break;
    }
  }

  /**
   * Resets each of the boolean variables that are used for displaying validation
   * messages
   */
  private resetValidationBools(): void {
    this.invalidForm = false;
    this.invalidCredentials = false;
    this.usernameAlreadyTaken = false;
    this.unexpectedError = false;
  }
}
