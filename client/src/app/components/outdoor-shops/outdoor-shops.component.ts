import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LocationType } from 'src/app/_enums/location-type';
import { INearbyPlace } from 'src/app/_interfaces/nearby-place';
import { LocationService } from 'src/app/_services/location.service';

@Component({
  selector: 'app-outdoor-shops',
  templateUrl: './outdoor-shops.component.html',
  styleUrls: ['./outdoor-shops.component.css']
})
export class OutdoorShopsComponent implements OnInit {
  @ViewChild('locationInput', { static: false }) locationInputRef: ElementRef;
  nearestOutdoorShops: INearbyPlace[] = [];

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.locationService.indoorLocationsSearchResult$
      .subscribe((res: INearbyPlace[]) => this.nearestOutdoorShops = res);
  }

  /**
   * Retrieves the nearest outdoor shops to the location searched for, via the location-service
   */
  findNearestOutdoorShops(): void {
    this.nearestOutdoorShops = null;
    const location = this.locationInputRef.nativeElement.value;
    this.locationService.getNearestLocations(location, LocationType.OutdoorShop);
  }
}
