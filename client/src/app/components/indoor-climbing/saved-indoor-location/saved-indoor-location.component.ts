import { Component, Input, OnInit } from '@angular/core';
import { IClimbingLocation } from 'src/app/_interfaces/climbing-location';
import { LocationService } from 'src/app/_services/location.service';

@Component({
  selector: 'app-saved-indoor-location',
  templateUrl: './saved-indoor-location.component.html',
  styleUrls: ['./saved-indoor-location.component.css']
})
export class SavedIndoorLocationComponent implements OnInit {
  @Input() location: IClimbingLocation;

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
  }

  /**
   * Removes this location from the user's saved indoor climbing locations
   */
  removeFromFavourites(): void {
    this.locationService.removeLocationForUser(this.location.locationId, false);
  }
}
