import { Component, Input, OnInit } from '@angular/core';
import { INearbyPlace } from 'src/app/_interfaces/nearby-place';
import { LocationService } from 'src/app/_services/location.service';

@Component({
  selector: 'app-indoor-location-search-result',
  templateUrl: './indoor-location-search-result.component.html',
  styleUrls: ['./indoor-location-search-result.component.css']
})
export class IndoorLocationSearchResultComponent implements OnInit {
  @Input() location: INearbyPlace;
  isAlreadySaved = false;

  constructor(
    private locationService: LocationService) { }

  ngOnInit(): void {
    this.checkIfLocationAlreadySaved();
  }

  /**
   * Checks if this search result is already saved by the user
   */
  checkIfLocationAlreadySaved() {
    for (let location of this.locationService.savedIndoorClimbingLocations) {
      if (location.name === this.location.name) {
        this.isAlreadySaved = true;
        break;
      }
    }
  }

  /**
   * Saves an indoor climbing location for a user
   */
  saveIndoorClimbingLocation(): void {
    this.locationService.saveClimbingLocation(this.location.placeId, false);
    this.isAlreadySaved = true;
  }
}
