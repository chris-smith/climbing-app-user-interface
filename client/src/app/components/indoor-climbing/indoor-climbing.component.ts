import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LocationType } from 'src/app/_enums/location-type';
import { IClimbingLocation } from 'src/app/_interfaces/climbing-location';
import { INearbyPlace } from 'src/app/_interfaces/nearby-place';
import { LocationService } from 'src/app/_services/location.service';
import { NavigationService } from 'src/app/_services/navigation.service';

@Component({
  selector: 'app-indoor-climbing',
  templateUrl: './indoor-climbing.component.html',
  styleUrls: ['./indoor-climbing.component.css']
})
export class IndoorClimbingComponent implements OnInit {
  @ViewChild('locationInput', { static: false }) locationInputRef: ElementRef;
  nearestClimbingCentres: INearbyPlace[] = [];
  savedClimbingCentres: IClimbingLocation[] = [];

  constructor(private locationService: LocationService, private navigationService: NavigationService) { }

  ngOnInit(): void {   
    this.locationService.indoorLocationsSearchResult$
      .subscribe((res: INearbyPlace[]) => {
        this.nearestClimbingCentres = res;
      });
    
    this.locationService.savedIndoorClimbingLocationsUpdated$
      .subscribe((locations: IClimbingLocation[]) => {
        this.savedClimbingCentres = locations;
      });

    this.navigationService.routeChange$.subscribe(() => this.nearestClimbingCentres.length = 0);

    if (this.savedClimbingCentres.length <= 0) {
      this.locationService.getSavedIndoorClimbingLocations()
    }
  }

  /**
   * Retrieves the nearest climbing centres to the location searched for, via the location-service
   */
  findNearestClimbingCentres(): void {
    this.nearestClimbingCentres = null;
    const location = this.locationInputRef.nativeElement.value;
    this.locationService.getNearestLocations(location, LocationType.ClimbingCentre);
  }
}
