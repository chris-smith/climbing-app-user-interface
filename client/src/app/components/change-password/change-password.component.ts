import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tapResponse } from '@ngrx/component-store';
import { map } from 'rxjs/operators';
import { Route } from 'src/app/_enums/route';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  invalidForm = false;
  invalidCredentials = false;
  unexpectedError = false;
  passwordUpdated = false;

  constructor(
    private fb: FormBuilder, 
    private router: Router,
    private authService: AuthService) {
    this.form = this.fb.group({
      username: ['',Validators.required],
      currentPassword: ['',Validators.required],
      newPassword: ['',Validators.required]
    });
   }

  ngOnInit(): void {
  }

  /**
   * Makes a request to the server to change a user's password
   */
  async changePassword(): Promise<void> {
    this.resetValidationBools();
    const currentFormValue = this.form.value;
    const username = currentFormValue.username;
    const currentPassword = currentFormValue.currentPassword;
    const newPassword = currentFormValue.newPassword;
    
    if (username && currentPassword && newPassword) {
      await this.authService.changePassword({ username: username, currentPassword: currentPassword, newPassword: newPassword })
      .pipe(
        tapResponse(
            () => {
              this.passwordUpdated = true;
              setTimeout(() => 
              {
                this.router.navigateByUrl(Route.OutdoorClimbing);
              },
              2000);
            },
            (err: HttpErrorResponse) => this.handleError(err)
        )
      ).subscribe();
    } else {
      this.invalidForm = true;
    }
  }

  /**
   * Resets each of the boolean variables that are used for displaying validation
   * messages
   */
   private resetValidationBools(): void {
    this.invalidForm = false;
    this.invalidCredentials = false;
    this.unexpectedError = false;
    this.passwordUpdated = false;
  }

  /**
   * Handles an error response from the UI server for a change-password request
   * @param err the HttpErrorResponse received from the UI server
   */
   private handleError(error: HttpErrorResponse): void {
    switch (error.status) {
      case 404:
        this.invalidCredentials = true;
        break;
      default:
        this.unexpectedError = true;
        console.log(`Unexpected error occurred. Response status: ${error.message}.`)
        break;
    }
  }
}
