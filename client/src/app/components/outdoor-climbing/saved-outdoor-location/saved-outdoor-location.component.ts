import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { ILocationWeather } from 'src/app/_interfaces/location-weather';
import { IWeatherForecastDialogData } from 'src/app/_interfaces/weather-forecast-dialog-data';
import { LocationService } from 'src/app/_services/location.service';
import { WeatherService } from 'src/app/_services/weather.service';
import { WeatherForecastDialogComponent } from '../weather-forecast-dialog/weather-forecast-dialog.component';

@Component({
  selector: 'app-saved-outdoor-location',
  templateUrl: './saved-outdoor-location.component.html',
  styleUrls: ['./saved-outdoor-location.component.css']
})
export class SavedOutdoorLocationComponent implements OnInit {
  @Input() locationWeather: ILocationWeather;

  constructor(
    private weatherService: WeatherService,
    private locationService: LocationService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  /**
   * Retrieves hourly and daily weather forecast data for the saved location
   */
  getWeatherForecasts(): void {
    this.weatherService.getWeatherForecasts(
      this.locationWeather.latitude, 
      this.locationWeather.latitude);
    
    combineLatest([this.weatherService.hourlyWeatherForecast$, this.weatherService.dailyWeatherForecast$])
      .pipe(take(1))
      .subscribe(([hourlyForecast, dailyForecast]) => {
        const weatherForecastDialogData: IWeatherForecastDialogData = {
          hourlyForecastData: hourlyForecast,
          dailyForecastData: dailyForecast
        };

        this.dialog.open(WeatherForecastDialogComponent, { data: weatherForecastDialogData })
    });
  }

  /**
   * Removes this location from the user's saved outdoor climbing locations
   */
  removeFromFavourites(): void {
    this.locationService.removeLocationForUser(this.locationWeather.locationId, true);
  }
}
