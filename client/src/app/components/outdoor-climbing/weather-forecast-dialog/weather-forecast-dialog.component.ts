import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IWeatherForecastDialogData } from 'src/app/_interfaces/weather-forecast-dialog-data';

@Component({
  selector: 'app-weather-forecast-dialog',
  templateUrl: './weather-forecast-dialog.component.html',
  styleUrls: ['./weather-forecast-dialog.component.css']
})
export class WeatherForecastDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<WeatherForecastDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: IWeatherForecastDialogData) { }

  ngOnInit(): void {
  }

  /**
   * Closes the dialog
   */
  close = () => this.dialogRef.close();
}
