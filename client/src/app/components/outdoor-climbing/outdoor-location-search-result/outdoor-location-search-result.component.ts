import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { ILocationDetails } from 'src/app/_interfaces/location-details';
import { IWeatherForecastDialogData } from 'src/app/_interfaces/weather-forecast-dialog-data';
import { LocationService } from 'src/app/_services/location.service';
import { WeatherService } from 'src/app/_services/weather.service';
import { WeatherForecastDialogComponent } from '../weather-forecast-dialog/weather-forecast-dialog.component';

@Component({
  selector: 'app-outdoor-location-search-result',
  templateUrl: './outdoor-location-search-result.component.html',
  styleUrls: ['./outdoor-location-search-result.component.css']
})
export class OutdoorLocationSearchResultComponent implements OnInit {
  @Input() location: ILocationDetails;
  @Input() isAlreadySaved: boolean;

  constructor(
    private locationService: LocationService, 
    private weatherService: WeatherService, 
    private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  /**
   * Saves the outdoor climbing location (search result) for a user
   */
  saveOutdoorClimbingLocation(): void {
    this.locationService.saveClimbingLocation(this.location.placeId, true);
    this.isAlreadySaved = true;
  }

  /**
   * Retrieves hourly and daily weather forecast data for the location displayed as 
   * a search result
   */
  getWeatherForecasts(): void {
    this.weatherService.getWeatherForecasts(
      this.location.weather.coordinates.latitude, 
      this.location.weather.coordinates.latitude);
    
    combineLatest([this.weatherService.hourlyWeatherForecast$, this.weatherService.dailyWeatherForecast$])
      .pipe(take(1))
      .subscribe(([hourlyForecast, dailyForecast]) => {
        const weatherForecastDialogData: IWeatherForecastDialogData = {
          hourlyForecastData: hourlyForecast,
          dailyForecastData: dailyForecast
        };

        this.dialog.open(WeatherForecastDialogComponent, { data: weatherForecastDialogData })
    });
  }
}
