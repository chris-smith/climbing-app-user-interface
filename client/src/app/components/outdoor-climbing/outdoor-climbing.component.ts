import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SocketMessageType } from 'src/app/_enums/socket-message-type';
import { ILocationDetails } from 'src/app/_interfaces/location-details';
import { ILocationWeather } from 'src/app/_interfaces/location-weather';
import { ISocketMessage } from 'src/app/_interfaces/socket-message';
import { IWeatherUpdateMessage } from 'src/app/_interfaces/weather-update-message';
import { AuthService } from 'src/app/_services/auth.service';
import { LocationService } from 'src/app/_services/location.service';
import { NavigationService } from 'src/app/_services/navigation.service';
import { SocketService } from 'src/app/_services/socket.service';

@Component({
  selector: 'app-outdoor-climbing',
  templateUrl: './outdoor-climbing.component.html',
  styleUrls: ['./outdoor-climbing.component.css']
})
export class OutdoorClimbingComponent implements OnInit, OnDestroy {
  @ViewChild('locationInput', { static: false }) locationInputRef: ElementRef;
  locationSearchResult: ILocationDetails;
  isSearchLocationAlreadySaved = false;
  savedOutdoorClimbingLocations: ILocationWeather[] = [];

  constructor(
    private locationService: LocationService, 
    private socketService: SocketService,
    private authService: AuthService,
    private navigationService: NavigationService) { }

  ngOnInit(): void {    
    this.socketService.joinRoom(this.authService.getUserId());
    this.locationService.outdoorLocationSearchResult$
      .subscribe((res: ILocationDetails) => {
        this.locationSearchResult = res;
        this.isSearchLocationAlreadySaved = this.savedOutdoorClimbingLocations.some(l => l.name === res.name);
      });
    
    this.locationService.savedOutdoorClimbingLocationsUpdated$
      .subscribe((locations: ILocationWeather[]) => {
        this.savedOutdoorClimbingLocations = locations;
      });

    this.socketService.newMessage$
      .subscribe((message: ISocketMessage) => {
        if (message.messageType === SocketMessageType.WeatherUpdate) {
          console.log("Received weather update message");
          const weatherUpdateMessage = message.message as IWeatherUpdateMessage;
          this.applyWeatherUpdates(weatherUpdateMessage.updates);
        }
      });

      this.navigationService.routeChange$.subscribe(() => this.locationSearchResult = null);
      if (this.savedOutdoorClimbingLocations.length <= 0) {
        this.locationService.getSavedOutdoorClimbingLocations();
      }
  }

  /**
   * Searches for the location input by the user, via the location service
   */
  searchForLocation() {
    this.locationSearchResult = null;
    const location = this.locationInputRef.nativeElement.value;
    this.locationService.searchForOutdoorLocation(location);
  }

  /**
   * Updates each saved outdoor climbing location with details received in a weather update message
   * @param weatherUpdates the weather updates received from the server
   */
  applyWeatherUpdates(weatherUpdates: ILocationWeather[]): void {
    for (let update of weatherUpdates) {
      for (let location of this.savedOutdoorClimbingLocations) {
        if (location.locationId === update.locationId) {
          this.updateLocationWeather(location, update);
        }
      }
    }
  }

  /**
   * Updates the first ILocationWeather object with the details from the supplied
   * second 
   * @param location the object to update
   * @param update the updated details to apply
   */
  updateLocationWeather(location: ILocationWeather, update: ILocationWeather) {
    location.name = update.name;
    location.locality = update.locality;
    location.country = update.country;
    location.postCode = update.postCode;
    location.latitude = update.latitude;
    location.longitude = update.longitude;
    location.condition = update.condition;
    location.temperature = update.temperature;
    location.feelsLikeTemp = update.feelsLikeTemp;
    location.humidity = update.humidity;
    location.windSpeed = update.windSpeed;
    location.windDirection = update.windDirection;
    location.lastUpdatedAt = update.lastUpdatedAt;
  }

  ngOnDestroy(): void {
    this.socketService.leaveRoom(this.authService.getUserId());
  }
}
