import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Route } from 'src/app/_enums/route';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.userAuthenticated.subscribe(
      (isUserAuthenticated: boolean) => {
        this.isLoggedIn = isUserAuthenticated;
      }
    )
  }

  /**
   * Logs the user out
   */
  logOff(): void {
    this.authService.logOut();    
    this.router.navigateByUrl(Route.Login);
    this.isLoggedIn = false;
  }

}
