import { Component, Input, OnInit } from '@angular/core';
import { INearbyPlace } from 'src/app/_interfaces/nearby-place';

@Component({
  selector: 'app-outdoor-shops-search-result',
  templateUrl: './outdoor-shops-search-result.component.html',
  styleUrls: ['./outdoor-shops-search-result.component.css']
})
export class OutdoorShopsSearchResultComponent implements OnInit {
  @Input() shop: INearbyPlace;

  constructor() { }

  ngOnInit(): void {
  }

}
