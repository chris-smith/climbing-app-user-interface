export enum LocationType {
    ClimbingCentre = "climbing-centre",
    OutdoorShop = "outdoor-shop"
}
