export enum ApiRoute {
    // Auth
    Register = "/api/auth/register",
    LogIn = "/api/auth/login",
    ChangePassword = "/api/auth/change-password",

    // Locations
    LocationSearch = "/api/location/location-search",
    SaveLocation = "/api/location/climbing-locations",
    RemoveLocation = "/api/location/climbing-locations",
    OutdoorClimbingLocations = "/api/location/climbing-locations/outdoor",
    IndoorClimbingLocations = "/api/location/climbing-locations/indoor",
    GetNearestLocations = "/api/location/nearby-locations",

    // Weather
    HourlyWeatherForecast = "/api/weather-forecast/hourly",
    DailyWeatherForecast = "/api/weather-forecast/daily",
    StartWeatherUpdates = "/api/weather-update/start",
    StopWeatherUpdates = "/api/weather-update/stop"
}
