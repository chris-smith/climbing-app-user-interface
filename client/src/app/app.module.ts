import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from "@angular/material/dialog";
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { IndoorClimbingComponent } from './components/indoor-climbing/indoor-climbing.component';
import { IndoorLocationSearchResultComponent } from './components/indoor-climbing/indoor-location-search-result/indoor-location-search-result.component';
import { SavedIndoorLocationComponent } from './components/indoor-climbing/saved-indoor-location/saved-indoor-location.component';
import { LoginComponent } from './components/login/login.component';
import { OutdoorClimbingComponent } from './components/outdoor-climbing/outdoor-climbing.component';
import { OutdoorLocationSearchResultComponent } from './components/outdoor-climbing/outdoor-location-search-result/outdoor-location-search-result.component';
import { SavedOutdoorLocationComponent } from './components/outdoor-climbing/saved-outdoor-location/saved-outdoor-location.component';
import { WeatherForecastDialogComponent } from './components/outdoor-climbing/weather-forecast-dialog/weather-forecast-dialog.component';
import { AppConstants } from './_constants/app-constants';
import { SettingsInitialisationService } from './_services/settings-initialisation-service.service';
import { OutdoorShopsComponent } from './components/outdoor-shops/outdoor-shops.component';
import { OutdoorShopsSearchResultComponent } from './components/outdoor-shops-search-result/outdoor-shops-search-result.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

export function appInit(settingsInitialisationService: SettingsInitialisationService) {
  return () => settingsInitialisationService.initialiseApp();
}

export function jwtTokenGetter(): string {
  return localStorage.getItem(AppConstants.LOCAL_STORAGE_AUTH_TOKEN_KEY);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OutdoorClimbingComponent,
    HeaderComponent,
    OutdoorLocationSearchResultComponent,
    SavedOutdoorLocationComponent,
    WeatherForecastDialogComponent,
    IndoorClimbingComponent,
    SavedIndoorLocationComponent,
    IndoorLocationSearchResultComponent,
    OutdoorShopsComponent,
    OutdoorShopsSearchResultComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter,
        allowedDomains: ["http://localhost:3000", "http://ui-server:3000"],
        disallowedRoutes: ["http://localhost:3000/api/auth", "http://ui-server:3000/api/auth"]
      }
    }),
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatTabsModule,
    MatGridListModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    IvyCarouselModule,
    AppRoutingModule
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: appInit, deps: [SettingsInitialisationService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
