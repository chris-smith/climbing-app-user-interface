import { ICoordinates } from "./coordinates";
import { IDaylightInfo } from "./daylight-info";
import { IWeatherMetrics } from "./weather-metrics";
import { IWeatherOverview } from "./weather-overview";
import { IWindInfo } from "./wind-info";

export interface ICurrentWeather {
    coordinates: ICoordinates;
    weatherOverview: IWeatherOverview[];
    weatherMetrics: IWeatherMetrics;
    visibility: number;
    windInfo: IWindInfo;
    daylightInfo: IDaylightInfo;
}
