import { IGeometryInfo } from "./geometry-info";

export interface INearbyPlace {
    placeId: string;
    name: string;
    formattedAddress: string;
    geometry: IGeometryInfo;
    businessStatus: string;
    userRatingAvg: string;
    userRatingsCount: string
}
