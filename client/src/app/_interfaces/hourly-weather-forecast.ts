import { IWeatherOverview } from "./weather-overview";

export interface IHourlyWeatherForecast {
    timeOfForecast: string;
    temperature: string;
    feelsLikeTemp: string;
    humidity: string;
    uvIndex: string;
    cloudiness: string;
    visibility: string;
    windSpeed: string;
    windDirection: string;
    chanceOfRain: string;
    weatherOverview: IWeatherOverview[];
}
