import { ILocationWeather } from "./location-weather";

export interface IWeatherUpdateMessage {
    userId: number;
    updates: ILocationWeather[];
}
