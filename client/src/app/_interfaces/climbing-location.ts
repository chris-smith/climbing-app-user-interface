export interface IClimbingLocation {
    locationId: number;
    isOutdoorLocation: boolean;
    name: string;
    locality: string;
    county: string;
    country: string;
    postCode: string;
    latitude: string;
    longitude: string;
}
