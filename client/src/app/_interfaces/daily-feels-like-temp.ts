export interface IDailyFeelsLikeTemp {
    dayFeelsLikeTemp: string;
    nightFeelsLikeTemp: string;
    morningFeelsLikeTemp: string;
    eveningFeelsLikeTemp: string;
}
