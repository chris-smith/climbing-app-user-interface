import { IDailyWeatherForecast } from "./daily-weather-forecast";
import { IHourlyWeatherForecast } from "./hourly-weather-forecast";

export interface IWeatherForecastDialogData {
    hourlyForecastData: IHourlyWeatherForecast[];
    dailyForecastData: IDailyWeatherForecast[];
}
