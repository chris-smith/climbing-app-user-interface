export interface ILocationWeather {
    locationId: number;
    name: string;
    locality: string;
    country: string;
    postCode: string;
    latitude: string;
    longitude: string;
    condition: string;
    temperature: string;
    feelsLikeTemp: string;
    humidity: string;
    windSpeed: string;
    windDirection: string;
    lastUpdatedAt: string;
}
