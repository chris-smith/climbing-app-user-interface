export interface IWindInfo {
    windSpeed: string;
    windDirection: string;
}
