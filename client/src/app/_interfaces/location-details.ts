import { ICurrentWeather } from "./current-weather";

export interface ILocationDetails {
    placeId: string;
    name: string;
    formattedAddress: string;
    weather: ICurrentWeather;
}
