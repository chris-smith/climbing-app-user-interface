export interface IWeatherOverview {
    condition: string;
    description: string;
}
