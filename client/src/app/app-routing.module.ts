import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { IndoorClimbingComponent } from './components/indoor-climbing/indoor-climbing.component';
import { LoginComponent } from './components/login/login.component';
import { OutdoorClimbingComponent } from './components/outdoor-climbing/outdoor-climbing.component';
import { OutdoorShopsComponent } from './components/outdoor-shops/outdoor-shops.component';
import { AuthGuardService as AuthGuard } from './_services/auth-guard.service';

const appRoutes: Routes = [
    { 
        path: 'login', 
        component: LoginComponent
    },
    {
        path: "outdoor-climbing",
        component: OutdoorClimbingComponent,
        canActivate: [AuthGuard]
    },
    {
      path: "indoor-climbing",
      component: IndoorClimbingComponent,
      canActivate: [AuthGuard]
    },
    {
      path: "outdoor-shops",
      component: OutdoorShopsComponent,
      canActivate: [AuthGuard]
    },
    {
      path: "change-password",
      component: ChangePasswordComponent,
      canActivate: [AuthGuard]
    },
    {
      path: '',
      redirectTo: '/outdoor-climbing',
      pathMatch: 'full'
    },
    {
      path: '**',
      redirectTo: '/outdoor-climbing',
      pathMatch: 'full'
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
