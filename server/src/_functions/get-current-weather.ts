import fetch from "node-fetch";
import queryString from "query-string";
import { HostnameHelper } from "../_helpers/hostname-helper";
import { ICurrentWeather } from "../_interfaces/current-weather";

const hostnameHelper = new HostnameHelper();

export const getCurrentWeather = async (latitude: string, longitude: string): Promise<ICurrentWeather> => {
    let weatherServiceHost: string;
    try {
        weatherServiceHost = await hostnameHelper.getHostname(process.env.WEATHER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Weather Service");
        console.error(e);
        return null;
    }

    console.log(`Retrieving current weather info for latitude ${latitude}, longitude ${longitude} via Weather Service`);
    let currentWeatherRes: any;
    const queryParams = queryString.stringify({ latitude, longitude });
    try {
        currentWeatherRes = await fetch(`http://${weatherServiceHost}:${process.env.WEATHER_SERVICE_PORT!}/current-weather?${queryParams}`);
    } catch (e) {
        console.error(`Failed to get current weather info for latitude ${latitude}, longitude ${longitude} from Weather Service. 
            Error details:`);
        console.error(e.stack);
        return null;
    }
    
    return await currentWeatherRes.json();
}
