import { Socket } from "socket.io";
import { API } from "../api/apis";

export const authentication = async (socket: Socket, next: any) => {
    console.log("Received a new websocket connection");
    const token = socket?.handshake?.query?.token;
    if (!token) {
        console.error(`Could not retrieve auth token for socket ${socket.id} - socket will be disconnected.`);
        socket.disconnect(true);
        return;
    }

    if (await API.auth.validateToken(token.toString())) {
        next();
    } else {
        console.error(`Token supplied in websocket request is invalid - request will 
            be rejected and the socket disconnected.`);
        socket.disconnect(true);
        return;
    }
}
