import { Socket } from "socket.io";

export const onConnect = (socket: Socket) => {
    socket.on("join", (rooms: string[]) => {
        socket.join(rooms);
        console.log(`Socket ${socket.id} joined the room`);
    });

    socket.on("leave", (room: string) => {
        socket.leave(room);
        console.log(`Socket ${socket.id} left the room`);
    });
}
