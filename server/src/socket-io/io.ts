const sio = require("socket.io");
let io: any = null;

exports.io = () => {
    return io;
}

exports.init = (server: any) => {
    return io = sio(server, {
        cors: {
            origin: '*',
        }
    });
}
