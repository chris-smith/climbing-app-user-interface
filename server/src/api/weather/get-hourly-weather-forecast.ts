import fetch from "node-fetch";
import queryString from "query-string";
import { capitaliseFirstLetter } from "../../_functions/capitalise-first-letter";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IHourlyWeatherForecast } from "../../_interfaces/hourly-weather-forecast";

const hostnameHelper = new HostnameHelper();

export const getHourlyWeatherForecast = async (req: any, res: any): Promise<any> => {
    console.log("Received request at hourly-weather-forecast endpoint.");
    const latitude = req?.query?.latitude;
    const longitude = req?.query?.longitude;
    if (!latitude) {
        res.statusMessage = `Missing required parameter "latitude"`;
        return res.status(400).end();
    } 
    if (!longitude) {
        res.statusMessage = `Missing required parameter "longitude"`;
        return res.status(400).end();
    }      

    let weatherServiceHost: string;
    try {
        weatherServiceHost = await hostnameHelper.getHostname(process.env.WEATHER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Weather Service");
        console.error(e);
        res.statusMessage = `An error occurred while processing request to get hourly weather forecast
            for latitude ${latitude}, longitude ${longitude}`;
        return res.status(500).end();
    }

    console.log(`Retrieving hourly weather forecast for latitude ${latitude}, longitude ${longitude} from Weather Service`);
    let hourlyForecastRes: any;
    const queryParams = queryString.stringify({ latitude, longitude });
    try {
        hourlyForecastRes = await fetch(`http://${weatherServiceHost}:${process.env.WEATHER_SERVICE_PORT!}/weather-forecast/hourly?${queryParams}`);
    } catch (e) {
        console.error(`Failed to retrieve hourly weather forecast for latitude ${latitude}, longitude ${longitude}. 
            Error details:`);
        console.error(e.stack);
        res.message = `Failed to retrieve hourly weather forecast for latitude ${latitude}, longitude ${longitude}`;
        return res.status(500).end();
    }

    const hourlyForecast: IHourlyWeatherForecast[] = (await hourlyForecastRes.json()).results;
    for (const forecast of hourlyForecast) {
        forecast.weatherOverview[0].description = capitaliseFirstLetter(forecast.weatherOverview[0].description);
    }

    return hourlyForecast ? res.status(200).json(hourlyForecast).end() : res.status(500).end();
}
