import fetch from "node-fetch";
import { getHttpHeaders } from "../../_functions/get-http-headers";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";

const hostnameHelper = new HostnameHelper();

/**
 * Stops weather updates for the user specified in the request
 * @param req the request object, containing the user's token/ID
 * @param res the response object
 */
export const stopWeatherUpdates = async (req: any, res: any) => {
    console.log("Received request at stop-weather-updates endpoint.")

    const userId = AuthTokenHelper.getUserIdFromToken(req?.body?.token);
    if (!userId) {
        res.statusMessage = `Missing or invalid auth token supplied in request.`;
        return res.status(400).end();
    } 

    let weatherServiceHost: string;
    try {
        weatherServiceHost = await hostnameHelper.getHostname(process.env.WEATHER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Weather Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing stop-weather-updates request";
        return res.status(500).end();
    }

    const url = `http://${weatherServiceHost}:${process.env.WEATHER_SERVICE_PORT!}/weather-update/${userId}`;
    const headers = getHttpHeaders();
    try {
        await fetch(url, { 
            method: "DELETE",
            headers: headers
        });
    } catch (e) {
        console.error(`Failed to stop weather updates for user ${userId}. Error details:`);
        console.error(e.stack);
        res.message = `Failed to stop weather updates for user ${userId}`;
        return res.status(500).end();
    }

    return res.status(200);
}
