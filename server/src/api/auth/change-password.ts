import { StatusCodes } from "http-status-codes";
import fetch from "node-fetch";
import { getHttpHeaders } from "../../_functions/get-http-headers";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IChangePasswordRequest } from "../../_interfaces/change-password-request";

const hostnameHelper = new HostnameHelper();

/**
 * Changes a user's password via the User Service
 * @param req the request object, containing the user's current credentials and desired password
 * @param res the response object
 */
export const changePassword = async (req: any, res: any) => {
    console.log("Received request at change-password endpoint.")

    const requestBody: IChangePasswordRequest = req?.body;
    const username = requestBody?.username;
    const currentPassword = requestBody?.currentPassword;
    const newPassword = requestBody?.newPassword;
    if (!username) {
        res.statusMessage = `Missing required parameter "username"`;
        return res.status(400).end();
    }
    if (!currentPassword) {
        res.statusMessage = `Missing required parameter "currentPassword"`;
        return res.status(400).end();
    }
    if (!newPassword) {
        res.statusMessage = `Missing required parameter "newPassword"`;
        return res.status(400).end();
    }

    let userServiceHost: string;
    try {
        userServiceHost = await hostnameHelper.getHostname(process.env.USER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for User Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing change-password request";
        return res.status(500).end();
    }

    const url = `http://${userServiceHost}:${process.env.USER_SERVICE_PORT!}/change-password`;
    const headers = getHttpHeaders();
    let userServiceRes: any;
    try {
        userServiceRes = await fetch(url, { 
            method: "POST",
            headers: headers, 
            body: JSON.stringify(requestBody) 
        });
    } catch (e) {
        userServiceRes.status = e?.statusCode;
    }

    switch (userServiceRes.status) {
        case StatusCodes.OK:
            return res.status(200).end();
        case StatusCodes.NOT_FOUND:
            console.log("Invalid username/current password combination supplied.")
            return res.status(404).end();
        case StatusCodes.INTERNAL_SERVER_ERROR:
            console.log("Received server error response from User Service.")
            return res.status(500).end();
        default:
            console.error(`Received unexpected response code from User Service. Code: ${userServiceRes.status}`)
            return res.status(500).end();
    }    
}
