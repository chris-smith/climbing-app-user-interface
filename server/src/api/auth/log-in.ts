import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import fetch from "node-fetch";
import queryString from "query-string";
import { GetAuthTokenResponse } from "../../_enums/get-auth-token-response";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { ILoginRequest } from "../../_interfaces/login-request";
import { ILogInResponse } from "../../_interfaces/login-response";

const hostnameHelper = new HostnameHelper();

/**
 * Authenticates the user with the supplied credentials and if valid, generates
 * and signs a JWT for the user
 * @param req the request object, containing the user's credentials
 * @param res the response object, containing the user's auth token
 */
export const logIn = async (req: any, res: Response<ILogInResponse>) => {
    console.log("Received request at login endpoint.")

    const requestBody: ILoginRequest = req?.body;
    const username = requestBody?.username;
    const password = requestBody?.password;
    if (!username) {
        res.statusMessage = `Missing required parameter "username"`;
        return res.status(400).end();
    }
    if (!password) {
        res.statusMessage = `Missing required parameter "password"`;
        return res.status(400).end();
    }

    let userServiceHost: string;
    try {
        userServiceHost = await hostnameHelper.getHostname(process.env.USER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for User Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing login request";
        return res.status(500).end();
    }

    const queryParams = queryString.stringify({ username, password });
    const userServiceRes = await fetch(`http://${userServiceHost}:${process.env.USER_SERVICE_PORT!}/login?${queryParams}`);
    let responseStatus: string;
    switch (userServiceRes.status) {
        case StatusCodes.OK:
            responseStatus = GetAuthTokenResponse.OK;
            break;
        case StatusCodes.NOT_FOUND:
            console.log("Supplied credentials were returned as invalid from User Service - user could not be authenticated.")
            return res.status(401).json({ responseStatus: GetAuthTokenResponse.INVALID_CREDENTIALS });
        default:
            console.error(`Received unexpected response code from User Service. Code: ${userServiceRes.status}`)
            return res.status(500).end();
    }

    const userId = await userServiceRes.json() as number;
    const token = AuthTokenHelper.generateAuthToken(userId);
    return res.status(200).json({
        responseStatus: responseStatus,
        token: token
    });
}
