import jwt from "jsonwebtoken";

/**
 * Validates the supplied auth token (JWT)
 * @param authToken the JWT supplied in a request
 * @returns a boolean indicating whether the supplied JWT is valid
 */
export const validateToken = async (authToken: string): Promise<boolean> => {
    try {
      jwt.verify(authToken, process.env.TOKEN_SECRET);
      return true;
    } catch(err) {
      console.error("Invalid auth token");
      return false;
    }
}
