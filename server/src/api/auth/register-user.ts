import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import fetch from "node-fetch";
import { GetAuthTokenResponse } from "../../_enums/get-auth-token-response";
import { getHttpHeaders } from "../../_functions/get-http-headers";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IRegisterUserRequest } from "../../_interfaces/register-user-request";
import { IRegisterUserResponse } from "../../_interfaces/register-user-response";

const hostnameHelper = new HostnameHelper();

/**
 * Registers a new user with the supplied credentials and if the username is not already
 * taken, generates and signs a JWT for the user
 * @param req the request object, containing the new user's credentials
 * @param res the response object, containing the new user's auth token
 */
export const registerUser = async (req: any, res: Response<IRegisterUserResponse>) => {
    console.log("Received request at register-user endpoint.")

    const requestBody: IRegisterUserRequest = req?.body;
    const username = requestBody?.username;
    const password = requestBody?.password;
    if (!username) {
        res.statusMessage = `Missing required parameter "username"`;
        return res.status(400).end();
    }
    if (!password) {
        res.statusMessage = `Missing required parameter "password"`;
        return res.status(400).end();
    }

    let userServiceHost: string;
    try {
        userServiceHost = await hostnameHelper.getHostname(process.env.USER_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for User Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing register-user request";
        return res.status(500).end();
    }

    const url = `http://${userServiceHost}:${process.env.USER_SERVICE_PORT!}/users`;
    const headers = getHttpHeaders();
    let userServiceRes: any;
    try {
        userServiceRes = await fetch(url, { 
            method: "POST",
            headers: headers, 
            body: JSON.stringify(requestBody) 
        });
    } catch (e) {
        userServiceRes.status = e?.statusCode;
    }

    let responseStatus: string;
    switch (userServiceRes.status) {
        case StatusCodes.OK:
            responseStatus = GetAuthTokenResponse.OK;
            break;
        case StatusCodes.CONFLICT:
            console.log(`Username ${username} already exists - user could not be created.`)
            return res.status(409).json({ responseStatus: GetAuthTokenResponse.USERNAME_ALREADY_TAKEN });
        default:
            console.error(`Received unexpected response code from User Service. Code: ${userServiceRes.status}`)
            return res.status(500).end();
    }

    const userId = await userServiceRes.json() as number;
    const token = AuthTokenHelper.generateAuthToken(userId);
    return res.status(200).json({
        responseStatus: responseStatus,
        token: token
    });
}
