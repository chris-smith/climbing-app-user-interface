import { changePassword } from "./auth/change-password";
import { logIn } from "./auth/log-in";
import { registerUser } from "./auth/register-user";
import { validateToken } from "./auth/validate-token";
import { getNearestLocations } from "./location/get-nearest-locations";
import { getSavedIndoorClimbingLocations } from "./location/get-saved-indoor-climbing-locations";
import { getSavedOutdoorClimbingLocations } from "./location/get-saved-outdoor-climbing-locations";
import { locationSearch } from "./location/location-search";
import { removeLocation } from "./location/remove-location";
import { saveLocation } from "./location/save-location";
import { getDailyWeatherForecast } from "./weather/get-daily-weather-forecast";
import { getHourlyWeatherForecast } from "./weather/get-hourly-weather-forecast";
import { startWeatherUpdates } from "./weather/start-weather-updates";
import { stopWeatherUpdates } from "./weather/stop-weather-updates";

export const API = {
    auth: {
        registerUser,
        logIn,
        validateToken,
        changePassword
    },
    location: {
        locationSearch,
        saveLocation,
        removeLocation,
        getSavedOutdoorClimbingLocations,
        getSavedIndoorClimbingLocations,
        getNearestLocations
    },
    weather: {
        getHourlyWeatherForecast,
        getDailyWeatherForecast,
        startWeatherUpdates,
        stopWeatherUpdates
    }
}
