import { StatusCodes } from "http-status-codes";
import fetch from "node-fetch";
import { getHttpHeaders } from "../../_functions/get-http-headers";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IRemoveLocationRequest } from "../../_interfaces/remove-location-request";

const hostnameHelper = new HostnameHelper();

export const removeLocation = async (req: any, res: any): Promise<any> => {
    console.log("Received request at remove-location endpoint.");
    const userId = AuthTokenHelper.getUserIdFromToken(req?.query?.token);
    const locationId = req?.query?.locationId;
    if (!userId || !locationId) {
        res.statusMessage = "Missing required parameter(s) - userId (token) and locationId must both be supplied in the request.";
        return res.status(400).end();
    }    

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing remove-location request";
        return res.status(500).end();
    }

    console.log(`Removing location ${locationId} from user ${userId}'s saved locations via Location Service`);
    const url = `http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/climbing-locations`;
    const headers = getHttpHeaders();
    const removeLocationReq: IRemoveLocationRequest = {
        userId: userId,
        locationId: locationId
    };
    
    let removeLocationRes: any;
    try {
        removeLocationRes = await fetch(url, { 
            method: "DELETE",
            headers: headers, 
            body: JSON.stringify(removeLocationReq) 
        });
    } catch (e) {
        console.error(`Failed to remove location ${locationId} for user ${userId}. Error details:`);
        console.error(e.stack);
        res.message = `Failed to remove location ${locationId} for user ${userId}`;
        return res.status(500).end();
    }

    switch (removeLocationRes.status) {
        case StatusCodes.OK:
            return res.status(200).end();
        case StatusCodes.INTERNAL_SERVER_ERROR:
            console.log("Received server error response from Location Service.")
            return res.status(500).end();
        default:
            console.error(`Received unexpected response code from Location Service. Code: ${removeLocationRes.status}`)
            return res.status(500).end();
    } 
}
