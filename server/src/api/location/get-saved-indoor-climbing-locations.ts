import fetch from "node-fetch";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IClimbingLocation } from "../../_interfaces/climbing-location";

const hostnameHelper = new HostnameHelper();

export const getSavedIndoorClimbingLocations = async (req: any, res: any): Promise<any> => {
    console.log("Received request at saved-indoor-climbing-locations endpoint.");
    const userId = AuthTokenHelper.getUserIdFromToken(req?.query?.token);
    if (!userId) {
        res.statusMessage = `Missing required parameter "userId"`;
        return res.status(400).end();
    }     

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing request to get a user's saved indoor climbing locations";
        return res.status(500).end();
    }

    console.log(`Retrieving saved indoor climbing locations for user ${userId} from Location Service`);
    let indoorLocationsRes: any;
    try {
        indoorLocationsRes = await fetch(`http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/climbing-locations/indoor/${userId}`);
    } catch (e) {
        console.error(`Failed to retrieve saved indoor climbing locations for user ${userId}. Error details:`);
        console.error(e.stack);
        res.message = `Failed to retrieve saved indoor climbing locations for user ${userId}`;
        return res.status(500).end();
    }

    const indoorLocations: IClimbingLocation[] = await indoorLocationsRes.json();
    if (!indoorLocations) {
        return res.status(500).end();
    }

    return res.status(200).json(indoorLocations).end();
}
