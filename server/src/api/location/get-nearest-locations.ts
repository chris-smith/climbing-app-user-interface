import fetch from "node-fetch";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { INearbyPlace } from "../../_interfaces/nearby-place";
import { IPlace } from "../../_interfaces/place";

const hostnameHelper = new HostnameHelper();

export const getNearestLocations = async (req: any, res: any): Promise<any> => {
    console.log("Received request at get-nearest-locations endpoint.");
    const locationName = req?.query?.locationName;
    const locationType = req?.query?.locationType;
    if (!locationName) {
        res.statusMessage = `Missing required parameter "locationName"`;
        return res.status(400).end();
    }     
    if (!locationType) {
        res.statusMessage = `Missing required parameter "locationType"`;
        return res.status(400).end();
    } 

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing get-nearest-locations request";
        return res.status(500).end();
    }

    console.log(`Retrieving location details for ${locationName} via Location Service`);
    let locationDetailsRes: any;
    try {
        locationDetailsRes = await fetch(`http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/location-search?name=${locationName}`);
    } catch (e) {
        console.error(`Failed to get location details for ${locationName} from Location Service. Error details:`);
        console.error(e.stack);
        res.message = `Failed to retrieve location details for ${locationName}`;
        return res.status(500).end();
    }

    const place: IPlace = await locationDetailsRes.json();
    console.log(`Retrieving nearest ${locationType}s to ${locationName} via Location Service`);
    let nearestLocationsRes: any;
    try {
        nearestLocationsRes = await fetch(`http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/nearby-locations/${locationType}?lat=${place.geometry.coordinates.latitude}&long=${place.geometry.coordinates.longitude}`);
    } catch (e) {
        console.error(`Failed to get nearest ${locationType}s to ${locationName} from Location Service. Error details:`);
        console.error(e.stack);
        res.message = `Failed to get nearest ${locationType}s to ${locationName}`;
        return res.status(500).end();
    }

    const nearbyPlaces: INearbyPlace[] = await nearestLocationsRes.json();
    if (!nearbyPlaces) {
        return res.status(500).end();
    }

    return res.status(200).json(nearbyPlaces).end();
}
