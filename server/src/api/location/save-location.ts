import fetch from "node-fetch";
import { capitaliseFirstLetter } from "../../_functions/capitalise-first-letter";
import { getCurrentWeather } from "../../_functions/get-current-weather";
import { getHttpHeaders } from "../../_functions/get-http-headers";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IClimbingLocation } from "../../_interfaces/climbing-location";
import { ICurrentWeather } from "../../_interfaces/current-weather";
import { ILocationWeather } from "../../_interfaces/location-weather";
import { INewClimbingLocationRequest } from "../../_interfaces/new-climbing-location-request";

const hostnameHelper = new HostnameHelper();

export const saveLocation = async (req: any, res: any): Promise<any> => {
    console.log("Received request at save-location endpoint.");
    const requestBody: INewClimbingLocationRequest = req?.body;
    const userId = AuthTokenHelper.getUserIdFromToken(requestBody?.userId);
    const placeId = requestBody?.placeId;
    if (!userId || !placeId || requestBody?.outdoorLocation == null) {
        res.statusMessage = "Missing required parameter(s) - userId (token), placeId and outdoorLocation must all be supplied in the request.";
        return res.status(400).end();
    }    

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing save-location request";
        return res.status(500).end();
    }

    console.log(`Saving climbing location with place ID ${placeId} for user ${userId} via Location Service`);
    const url = `http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/climbing-locations`;
    const headers = getHttpHeaders();
    requestBody.userId = userId;
    let saveLocationRes: any;
    try {
        saveLocationRes = await fetch(url, { 
            method: "POST",
            headers: headers, 
            body: JSON.stringify(requestBody) 
        });
    } catch (e) {
        console.error(`Failed to save location with place ID ${placeId} for user ${userId}. Error details:`);
        console.error(e.stack);
        res.message = `Failed to save location with place ID ${placeId} for user ${userId}`;
        return res.status(500).end();
    }

    const savedLocation: IClimbingLocation = await saveLocationRes.json();
    if (!savedLocation) {
        return res.status(500).end();
    }

    if (!requestBody.outdoorLocation) {
        return res.status(200).json(savedLocation).end();
    }

    const currentWeather: ICurrentWeather = await getCurrentWeather(savedLocation.latitude, savedLocation.longitude);
    if (!currentWeather) {
        return res.status(500).end();
    }

    const locationWeather: ILocationWeather = {
        locationId: savedLocation.locationId,
        name: savedLocation.name,
        locality: savedLocation.locality,
        country: savedLocation.country,
        postCode: savedLocation.postCode,
        latitude: savedLocation.latitude,
        longitude: savedLocation.longitude,
        condition: capitaliseFirstLetter(currentWeather.weatherOverview[0].description),
        temperature: currentWeather.weatherMetrics.temperature,
        feelsLikeTemp: currentWeather.weatherMetrics.feelsLikeTemp,
        humidity: currentWeather.weatherMetrics.humidity,
        windSpeed: currentWeather.windInfo.windSpeed,
        windDirection: currentWeather.windInfo.windDirection,
        lastUpdatedAt: new Date().toLocaleString()
    };

    return res.status(200).json(locationWeather).end();
}
