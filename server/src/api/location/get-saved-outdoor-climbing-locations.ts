import fetch from "node-fetch";
import { capitaliseFirstLetter } from "../../_functions/capitalise-first-letter";
import { getCurrentWeather } from "../../_functions/get-current-weather";
import { AuthTokenHelper } from "../../_helpers/auth-token-helper";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { IClimbingLocation } from "../../_interfaces/climbing-location";
import { ICurrentWeather } from "../../_interfaces/current-weather";
import { ILocationWeather } from "../../_interfaces/location-weather";

const hostnameHelper = new HostnameHelper();

export const getSavedOutdoorClimbingLocations = async (req: any, res: any): Promise<any> => {
    console.log("Received request at saved-outdoor-climbing-locations endpoint.");
    const userId = AuthTokenHelper.getUserIdFromToken(req?.query?.token);
    if (!userId) {
        res.statusMessage = `Missing required parameter "userId"`;
        return res.status(400).end();
    }     

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing request to get a user's saved outdoor climbing locations";
        return res.status(500).end();
    }

    console.log(`Retrieving saved outdoor climbing locations for user ${userId} from Location Service`);
    let outdoorLocationsRes: any;
    try {
        outdoorLocationsRes = await fetch(`http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/climbing-locations/outdoor/${userId}`);
    } catch (e) {
        console.error(`Failed to retrieve saved outdoor climbing locations for user ${userId}. Error details:`);
        console.error(e.stack);
        res.message = `Failed to retrieve saved outdoor climbing locations for user ${userId}`;
        return res.status(500).end();
    }

    const outdoorLocations: IClimbingLocation[] = await outdoorLocationsRes.json();
    const locationWeatherList: ILocationWeather[] = [];
    for (const location of outdoorLocations) {
        const currentWeather: ICurrentWeather = await getCurrentWeather(location.latitude, location.longitude);
        if (!currentWeather) {
            return res.status(500).end();
        }

        const locationWeather: ILocationWeather = {
            locationId: location.locationId,
            name: location.name,
            locality: location.locality,
            country: location.country,
            postCode: location.postCode,
            latitude: location.latitude,
            longitude: location.longitude,
            condition: capitaliseFirstLetter(currentWeather.weatherOverview[0].description),
            temperature: currentWeather.weatherMetrics.temperature,
            feelsLikeTemp: currentWeather.weatherMetrics.feelsLikeTemp,
            humidity: currentWeather.weatherMetrics.humidity,
            windSpeed: currentWeather.windInfo.windSpeed,
            windDirection: currentWeather.windInfo.windDirection,
            lastUpdatedAt: new Date().toLocaleString()
        };
        locationWeatherList.push(locationWeather);
    }

    return res.status(200).json(locationWeatherList).end();
}
