import fetch from "node-fetch";
import { capitaliseFirstLetter } from "../../_functions/capitalise-first-letter";
import { getCurrentWeather } from "../../_functions/get-current-weather";
import { HostnameHelper } from "../../_helpers/hostname-helper";
import { ICurrentWeather } from "../../_interfaces/current-weather";
import { ILocationDetails } from "../../_interfaces/location-details";
import { IPlace } from "../../_interfaces/place";

const hostnameHelper = new HostnameHelper();

export const locationSearch = async (req: any, res: any): Promise<any> => {
    console.log("Received request at location-search endpoint.");
    const locationName = req?.query?.locationName;
    if (!locationName) {
        res.statusMessage = `Missing required parameter "locationName"`;
        return res.status(400).end();
    }    

    let locationServiceHost: string;
    try {
        locationServiceHost = await hostnameHelper.getHostname(process.env.LOCATION_SERVICE_HOST!);
    } catch (e) {
        console.error("Failed to get the hostname for Location Service");
        console.error(e);
        res.statusMessage = "An error occurred while processing location-search request";
        return res.status(500).end();
    }

    console.log(`Retrieving location details for ${locationName} via Location Service`);
    let locationSearchRes: any;
    try {
        locationSearchRes = await fetch(`http://${locationServiceHost}:${process.env.LOCATION_SERVICE_PORT!}/location-search?name=${locationName}`);
    } catch (e) {
        console.error(`Failed to get location details for ${locationName} from Location Service. Error details:`);
        console.error(e.stack);
        res.message = `Failed to retrieve location details for ${locationName}`;
        return res.status(500).end();
    }

    const place: IPlace = await locationSearchRes.json();
    const currentWeather: ICurrentWeather = await getCurrentWeather(
        place.geometry.coordinates.latitude, 
        place.geometry.coordinates.longitude);

    if (!currentWeather) {
        return res.status(500).end();
    }

    currentWeather.weatherOverview[0].description = capitaliseFirstLetter(currentWeather.weatherOverview[0].description);
    const locationDetails: ILocationDetails = {
        placeId: place.placeId,
        name: place.name,
        formattedAddress: place.formattedAddress,
        weather: currentWeather
    };

    return res.status(200).json(locationDetails).end();
}
