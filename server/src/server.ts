require("dotenv").config();
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { appInfo } from "./app-info/app-info";
import { createKafkaConsumer } from "./kafka/kafka-consumer";
import { addRoutes } from "./routes";
import { authentication } from "./socket-io/authentication";
import { onConnect } from "./socket-io/on-connect";
import { RunMode } from "./_enums/run-mode";

console.log(`Version: ${appInfo.version}`)

const RUN_MODE = process?.env?.MODE;
if (!RUN_MODE) {
    throw new Error("No run mode specified - server cannot start up.");
}

switch (RUN_MODE) {
    case RunMode.Dev:
        console.log("Running server in development mode");
        break;
    default:
        throw new Error(`Run mode ${RUN_MODE} not known`);
}

// Configure Kafka
createKafkaConsumer();

// Configure application
const PORT = process?.env?.SERVER_PORT! || "3000";
const app = express();
const server = require("http").createServer(app);

// Add common middleware
app.use(cors());
app.use(express.json());
app.use(bodyParser.json());

// Add routes
addRoutes(app);

// Configure Socket.IO
const io = require("./socket-io/io").init(server);
io.use(authentication);
io.on("connection", onConnect);

server.listen(PORT, () => console.log(`Listening on port: ${PORT}`));
