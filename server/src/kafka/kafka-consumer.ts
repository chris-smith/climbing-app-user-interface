import { Constants } from "../constants";
import { Topic } from "../_enums/topic";
import { IKafkaMessage } from "../_interfaces/kafka-message";
import { handleWeatherUpdateMessage } from "./kafka-message-handlers/weather-update-message-handler";

const kafka = require("kafka-node");

export const createKafkaConsumer = (): void => {
    console.log("Starting Kafka consumer");
    const client = new kafka.KafkaClient({ kafkaHost: `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}` });
    const consumer = new kafka.Consumer(
        client,
        [{ topic: Topic.WeatherUpdate, partition: 0 }],
        {
            encoding: Constants.KafkaConsumerEncodingType
        }
    );
    
    consumer.on("message", (message: IKafkaMessage) => {
        let deserialisedMessage: any;
        try {
            deserialisedMessage = JSON.parse(message.value);
        } catch (SyntaxError) {
            console.error(`Invalid message received on ${message.topic} topic - could not parse JSON. 
                Message received on topic: ${message}`)
        }

        if (message.topic === Topic.WeatherUpdate) {
            console.log(`Message received on ${Topic.WeatherUpdate} topic`);
            handleWeatherUpdateMessage(deserialisedMessage);
        }
    });
}
