import { SocketMessageType } from "../../_enums/socket-message-type";
import { capitaliseFirstLetter } from "../../_functions/capitalise-first-letter";
import { ILocationWeather } from "../../_interfaces/location-weather";
import { ISocketMessage } from "../../_interfaces/socket-message";
import { IWeatherUpdateMessageBackend } from "../../_interfaces/weather-update-message-backend";
import { IWeatherUpdateMessageClient } from "../../_interfaces/weather-update-message-client";

export const handleWeatherUpdateMessage = (message: IWeatherUpdateMessageBackend) => {
    const locationWeatherUpdates: ILocationWeather[] = [];
    for (const update of message.updates) {
        const locationWeather: ILocationWeather = {
            locationId: update.location.locationId,
            name: update.location.name,
            locality: update.location.locality,
            country: update.location.country,
            postCode: update.location.postCode,
            latitude: update.location.latitude,
            longitude: update.location.longitude,
            condition: capitaliseFirstLetter(update.currentWeather.weatherOverview[0].description),
            temperature: update.currentWeather.weatherMetrics.temperature,
            feelsLikeTemp: update.currentWeather.weatherMetrics.feelsLikeTemp,
            humidity: update.currentWeather.weatherMetrics.humidity,
            windSpeed: update.currentWeather.windInfo.windSpeed,
            windDirection: update.currentWeather.windInfo.windDirection,
            lastUpdatedAt: new Date().toLocaleString()
        };

        locationWeatherUpdates.push(locationWeather);        
    }

    if (!locationWeatherUpdates.length) {
        console.log("No data could be extracted from weather update message")
        return;
    }

    const weatherUpdateForClient: IWeatherUpdateMessageClient = {
        userId: message.userId,
        updates: locationWeatherUpdates
    };

    const socketMessage: ISocketMessage = {
        messageType: SocketMessageType.WeatherUpdate,
        message: weatherUpdateForClient
    };

    require("../../socket-io/io").io().sockets.in(message.userId).emit("message", socketMessage);
}
