import { API } from "../api/apis";

export const authMiddleware = async (req: any, res: any, next: any) => {
    const authToken = req?.query?.token ?? req?.body?.token ?? req?.body?.userId;
    if (!authToken) {
        console.error("No auth token supplied in request");
        return res.status(401).end();
    }

    let validToken = false;
    try {
        validToken = await API.auth.validateToken(authToken);
    } catch (e) {
        console.error(`Error occurred while trying to validate auth token: ${e}`);
        return res.status(500).end();
    }

    if (!validToken) {
        console.error("Invalid auth token");
        return res.status(401).end();
    }

    return next();
}
