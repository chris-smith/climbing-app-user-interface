export const Constants = {
    KafkaConsumerEncodingType: "buffer",
    DefaultWeatherUpdateIntervalSeconds: 30
}
