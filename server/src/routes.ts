import { API } from "./api/apis";
import { authMiddleware } from "./middleware/auth-middleware";
import { Route } from "./_enums/route";

export const addRoutes = (app: any) => {
    // Auth
    app.route(Route.RegisterUser).post(API.auth.registerUser);
    app.route(Route.LogIn).post(API.auth.logIn);
    app.route(Route.ChangePassword).post(API.auth.changePassword);
    
    // Locations
    app.route(Route.LocationSearch).get(authMiddleware, API.location.locationSearch);
    app.route(Route.SaveLocation).post(authMiddleware, API.location.saveLocation);
    app.route(Route.RemoveLocation).delete(authMiddleware, API.location.removeLocation);
    app.route(Route.OutdoorClimbingLocations).get(authMiddleware, API.location.getSavedOutdoorClimbingLocations);
    app.route(Route.IndoorClimbingLocations).get(authMiddleware, API.location.getSavedIndoorClimbingLocations);
    app.route(Route.NearestLocations).get(authMiddleware, API.location.getNearestLocations);

    // Weather
    app.route(Route.HourlyWeatherForecast).get(authMiddleware, API.weather.getHourlyWeatherForecast);
    app.route(Route.DailyWeatherForecast).get(authMiddleware, API.weather.getDailyWeatherForecast);
    app.route(Route.StartWeatherUpdates).post(authMiddleware, API.weather.startWeatherUpdates);
    app.route(Route.StopWeatherUpdates).post(authMiddleware, API.weather.stopWeatherUpdates);
}
