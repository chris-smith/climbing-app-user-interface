import jwt from "jsonwebtoken";

export class AuthTokenHelper {
    
    /**
     * Generates and signs a JWT for the user with the supplied ID
     * @param userId the ID of user that the JWT is for
     */
    static generateAuthToken(userId: number): string {
        const tokenExpiresInSeconds = +process.env.TOKEN_EXPIRY_MINS! * 60;
        return jwt.sign(
            { userId: userId }, 
            process.env.TOKEN_SECRET, 
            { expiresIn: `${tokenExpiresInSeconds}s` }
        );
    }

    /**
     * Extracts the user ID from the supplied auth token
     * @param token a user's JWT
     */
    static getUserIdFromToken(token: string) {
        const decodedToken = jwt.decode(token);
        return decodedToken["userId"];
    }
}
