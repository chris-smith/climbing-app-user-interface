import { RunMode } from "../_enums/run-mode";
import { eurekaClientManager } from "./eureka-helper";

export class HostnameHelper {
    
    /**
     * Gets the hostname that should be used when to trying to make HTTP
     * requests to the specified application
     * @param appName the name of the application/microservice to get the
     * hostname for
     */
    async getHostname(appName: string) {
        const runMode = process.env.MODE;
        if (runMode !== RunMode.Dev) {
            return this.getMicroserviceIpAddress(appName);
        }

        return appName;
    }

    /**
     * Gets an IP address of the specified microservice from Eureka
     * @param appName the name of the application/microservice to retrieve an
     * IP address for
     */
    private async getMicroserviceIpAddress(appName: string): Promise<string> {
        const eurekaClient: any = await eurekaClientManager.getClient();
        const serviceInstance = eurekaClient.getInstancesByAppId(appName)[0];
        if (!serviceInstance) {
            throw new Error(`No instance of ${appName} could be found by Eureka.`)
        }

        return serviceInstance.ipAddr;
    }
}
