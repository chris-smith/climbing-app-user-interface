let eurekaClient: any;

/**
 * Manages Eureka clients
 */
export const eurekaClientManager = {

    /**
     * Registers a new client with Eureka
     */
    registerClient() {
        // Not implemented yet
    },
    
    /**
     * Gets the configured Eureka client
     */
    getClient() {
        if (!eurekaClient) {
            throw new Error("Eureka client has not been initialised yet - call registerClient() first.")
        }

        return eurekaClient;
    }
}
