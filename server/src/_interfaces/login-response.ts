export interface ILogInResponse {
    responseStatus: string,
    token?: string
}
