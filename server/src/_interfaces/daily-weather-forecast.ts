import { IDailyFeelsLikeTemp } from "./daily-feels-like-temp";
import { IDailyTemp } from "./daily-temp";
import { IWeatherOverview } from "./weather-overview";

export interface IDailyWeatherForecast {
    timeOfForecast: string;
    sunrise: string;
    sunset: string;
    temperature: IDailyTemp;
    feelsLikeTemp: IDailyFeelsLikeTemp;
    humidity: string;
    uvIndex: string;
    cloudiness: string;
    windSpeed: string;
    windDirection: string;
    chanceOfRain: string;
    weatherOverview: IWeatherOverview[];
}
