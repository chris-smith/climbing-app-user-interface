import { SocketMessageType } from "../_enums/socket-message-type";

export interface ISocketMessage {
    messageType: SocketMessageType;
    message: any;
}
