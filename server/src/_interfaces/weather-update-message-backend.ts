import { IWeatherUpdate } from "./weather-update";

export interface IWeatherUpdateMessageBackend {
    userId: number;
    updates: IWeatherUpdate[];
}
