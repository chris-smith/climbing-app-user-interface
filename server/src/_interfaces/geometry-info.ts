import { ICoordinates } from "./coordinates";

export interface IGeometryInfo {
    coordinates: ICoordinates;
}
