export interface IRegisterUserResponse {
    responseStatus: string,
    token?: string
}
