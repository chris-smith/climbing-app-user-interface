export interface IDaylightInfo {
    sunrise: string;
    sunset: string;
}
