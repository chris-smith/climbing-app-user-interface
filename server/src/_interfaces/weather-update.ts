import { IClimbingLocation } from "./climbing-location";
import { ICurrentWeather } from "./current-weather";

export interface IWeatherUpdate {
    location: IClimbingLocation;
    currentWeather: ICurrentWeather;
}
