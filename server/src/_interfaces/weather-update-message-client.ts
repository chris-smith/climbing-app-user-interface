import { ILocationWeather } from "./location-weather";

export interface IWeatherUpdateMessageClient {
    userId: number;
    updates: ILocationWeather[];
}
