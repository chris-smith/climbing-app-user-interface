export interface IRegisterUserRequest {
    username: string;
    password: string;
}
