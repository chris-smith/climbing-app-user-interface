import { IGeometryInfo } from "./geometry-info";

export interface IPlace {
    placeId: string;
    name: string;
    formattedAddress: string;
    geometry: IGeometryInfo;
}
