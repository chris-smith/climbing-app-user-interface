export interface IKafkaMessage {
    topic: string,
    value: any,
    offset: number,
    partition: number,
    highWaterOffset: number,
    key: null
}
