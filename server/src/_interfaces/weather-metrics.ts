export interface IWeatherMetrics {
    temperature: string;
    feelsLikeTemp: string;
    minTemp: string;
    maxTemp: string;
    humidity: string;
}
