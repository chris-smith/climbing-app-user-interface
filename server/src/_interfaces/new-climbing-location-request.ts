export interface INewClimbingLocationRequest {
    userId: string;
    placeId: string;
    outdoorLocation: boolean;
}
