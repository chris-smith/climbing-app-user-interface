export interface IRemoveLocationRequest {
    userId: string;
    locationId: number;
}
