export interface IDailyTemp {
    dayTemp: string;
    minTemp: string;
    maxTemp: string;
    nightTemp: string;
    morningTemp: string;
    eveningTemp: string;
}
